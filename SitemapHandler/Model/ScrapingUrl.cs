using System;
using System.Runtime.Serialization;

namespace SitemapHandler.Model
{
    public class ScrapingUrl : Uri
    {
        public DateTime Since { get; private set; }
        public DateTime Until { get; private set; }
        public Uri Domain { get; protected set; }
        public DateTime ModifiedDate { get; protected set; }
        public int Retries { get; internal set; } = 0;
        protected ScrapingUrl(string uriString) : base(uriString) { }
        public ScrapingUrl(string uriString, Uri domain, DateTime since, DateTime until) : base(uriString)
        {
            Since = since;
            Until = until;
            Domain = domain;
        }
    }
}
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Threading;

namespace SitemapHandler.Model
{
    public class ScrapingInternalData
    {
        public ConcurrentQueue<ScrapingUrl> Queue = new ConcurrentQueue<ScrapingUrl>();
        public Uri Domain { get; internal set; }
        public int Count { get;  set; }
        [JsonIgnore]
        public SemaphoreSlim DomainSemaphore { get; } = new SemaphoreSlim(1, 1);

        public ScrapingInternalData( Uri domain)
        {
            this.Domain = domain;
        }

        private int TaskCount = 0;
        internal int IncreaseTaskCount()
        {
            return Interlocked.Increment(ref TaskCount);
        }

        internal int DecreaseTaskCount()
        {
            return Interlocked.Decrement(ref TaskCount);
        }
    }
}
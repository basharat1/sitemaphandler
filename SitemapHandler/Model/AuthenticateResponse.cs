using SitemapHandler.Entities;
using System;
namespace SitemapHandler.Model

{
    public class AuthenticateResponse
    {
    
        public string Token { get; set; }
        public DateTime? Expire { get; set; }


        public AuthenticateResponse(string this_token, DateTime? this_Expire)
        {
        
            Token = this_token;
            Expire = this_Expire;
        }
    }
}
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SitemapHandler.Model

{
    public abstract class GeneralResponse
    {
        public int Code { get; set; }
        public string Description { get; set; }
        public object Payload { get; set; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(
                this,
                new JsonSerializerOptions
                {
                    DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault,
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                });
        }
    }

    public class ErrorResponse : GeneralResponse
    {
        public ErrorResponse(int Code, string Description) : this(Code, Description, null) { }

        public ErrorResponse(int Code, string Description, object Payload)
        {
            this.Code = Code;
            this.Description = Description;
            this.Payload = Payload;
        }
    }

    public class OkResponse : ErrorResponse
    {
        public OkResponse(object Payload) : base(Constants.ApiErrors.SUCCESS,
                                                 "Success",
                                                 Payload)
        { }
    }
}
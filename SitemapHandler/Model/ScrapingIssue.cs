using System;
namespace SitemapHandler.Model
{
    public class ScrapingIssue
    {
        public int Code { get; set; }
        public string Description { get; set; }
        public Uri Uri { get; set; }
        public Uri Domain { get; internal set; }
    }
}
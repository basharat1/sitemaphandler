using System;

namespace SitemapHandler.Model
{
    public class SitemapUrl : ScrapingUrl
    {
        public SitemapUrl(string uriString, Uri domain, DateTime since, DateTime until) : base(uriString, domain, since, until) { }
    }
}
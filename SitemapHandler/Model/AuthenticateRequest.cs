using System.ComponentModel.DataAnnotations;

namespace SitemapHandler.Model

{
    public class AuthenticateRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
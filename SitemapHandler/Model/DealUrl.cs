using System;

namespace SitemapHandler.Model
{
    public class DealUrl : ScrapingUrl
    {
        public DealUrl(string uriString, Uri domain, DateTime urlModifiedDate) : base(uriString)
        {
            base.ModifiedDate = urlModifiedDate;
            base.Domain = domain;
        }
    }
}
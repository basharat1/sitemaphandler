using System;

namespace SitemapHandler.Model
{
    public class ScrapedInfo
    {
        //ASIN found in a url on the page
        public string Asin { get; set; }

        //The url where the ASIN was found
        public string ProductUrl { get; set; }

        //The page title
        public string Title { get; set; }

        //The last modified date of the page according to sitemap information
        public DateTime ModifiedDate { get; set; }

        //The scraped url
        public Uri Url { get; set; }

        //The domain scraped as a starting point
        public Uri Domain { get; set; }

        //Additional info to send between delegates
        public object Bag {get; set;}
    }
}
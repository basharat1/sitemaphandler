﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SitemapHandler.Model
{
    public class ScraperServiceResponse
    {
            public int Code { get; set; }
            public string Message { get; set; }
            public object expires_in { get; set; }
            public string Access_token { get; set; }
            public string scope { get; set; }
            public string Token_type { get; set; }
        
    }


    public class Asin
    {
        public List<string> asin { get; set; }
    }
    public class Result
    {
        [JsonProperty("asins")]
        public JObject Asins { get; set; }

        [JsonProperty("html")]
        public string Html { get; set; }
    }

    public class ScraperServiceResponseForResults
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("error")]
        public object Error { get; set; }

        [JsonProperty("result")]
        public Result Result { get; set; }
    }

    //public class ScraperServiceResponseForResults
    //{
    //    public string Url { get; set; }
    //    public string Error { get; set; }
    //    public Result Result { get; set; }
    //    public string Code { get; set; }
    //}
    //public class Result
    //{
    //    public string html { get; set; }
    //    public string Asins { get; set; }
    //}
}

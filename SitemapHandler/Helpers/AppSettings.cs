using System.Collections.Generic;

namespace SitemapHandler.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string ConnectionString { get; set; }
     
    }
}
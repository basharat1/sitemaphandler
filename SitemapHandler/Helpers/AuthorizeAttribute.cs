using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Authorization;
using SitemapHandler.Entities;
using SitemapHandler.Model;

namespace SitemapHandler.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        //private readonly IList<Role> _roles;

        //public AuthorizeAttribute(params Role[] roles)
        //{
        //    _roles = roles ?? new Role[] { };
        //}
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            // skip authorization if action is decorated with [AllowAnonymous] attribute
            var allowAnonymous = context.ActionDescriptor.EndpointMetadata.OfType<AllowAnonymousAttribute>().Any();
            if (allowAnonymous)
                return;

            // authorization
            //var user = (Client)context.HttpContext.Items["Client"];  
            var user = context.HttpContext.Items["Client"];

            if (user == null)
            {
                // not logged in
                var x = new ErrorResponse( StatusCodes.Status401Unauthorized, $"Unauthorized");
                context.Result = new JsonResult(x) { StatusCode = StatusCodes.Status401Unauthorized };
            }
            else if(Convert.ToString(user).ToLower().Contains("jwt")==true)
            {
                var x = new ErrorResponse(StatusCodes.Status401Unauthorized, $"Jwt_Expire");
                context.Result = new JsonResult(x) { StatusCode = StatusCodes.Status401Unauthorized };
            }

        }
    }
}
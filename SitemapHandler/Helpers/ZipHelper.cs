using System;
using System.Diagnostics;
namespace SitemapHandler.Helpers
{
    public class ZipHelper
    {
        private const int ZIP_LEAD_BYTES = 0x04034b50;
        private const ushort GZIP_LEAD_BYTES = 0x8b1f;


        public static bool IsPkZipCompressedData(byte[] data)
        {
            Debug.Assert(data != null && data.Length >= 4);
            // if the first 4 bytes of the array are the ZIP signature then it is compressed data
            return (BitConverter.ToInt32(data, 0) == ZIP_LEAD_BYTES);
        }

        public static bool IsGZipCompressedData(byte[] data)
        {
            Debug.Assert(data != null && data.Length >= 2);
            // if the first 2 bytes of the array are theG ZIP signature then it is compressed data;
            return (BitConverter.ToUInt16(data, 0) == GZIP_LEAD_BYTES);
        }

        public static bool IsCompressedData(byte[] data)
        {
            return IsPkZipCompressedData(data) || IsGZipCompressedData(data);
        }

    }
}
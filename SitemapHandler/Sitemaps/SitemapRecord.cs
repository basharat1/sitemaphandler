namespace SitemapHandler.Sitemaps
{
    public class SitemapRecord
    {
        public string loc { get; set; }
        public string lastmod { get; set; }
        public LocType type { get; set; }
    }
}
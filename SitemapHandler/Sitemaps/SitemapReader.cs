using System.IO;
using System.Collections.Generic;
using System.Threading;

namespace SitemapHandler.Sitemaps
{
    public abstract class SitemapReader
    {
        public string Domain { get; }
        protected SitemapReader(string domain)
        {
            Domain = domain;
        }

        public abstract IEnumerable<SitemapRecord> GetLinks(Stream stream, CancellationTokenSource cancelToken);
    }
}
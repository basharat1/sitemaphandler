using SitemapHandler.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;

//using Lib.Helpers;

namespace SitemapHandler.Sitemaps
{
    public class Handler
    {
        private Stream _stream;
        private SitemapReader _sitemapReader;
        private Uri _url;

        public string Domain
        {
            get { return _url.Host; }
        }

        public Handler(Stream stream, Uri url)
        {
            this._stream = stream;
            this._url = url;

            stream.Seek(0, 0);

            try
            {
                if (url.AbsolutePath == "/robots.txt")
                    _sitemapReader = new RobotsSitemap(Domain);
                else
                    _sitemapReader = new FlatSitemap(Domain);


                byte[] bytes = new byte[4];
                stream.Read(bytes, 0, 4);
                if (ZipHelper.IsGZipCompressedData(bytes))
                    _sitemapReader = new GZipUncompressor(_sitemapReader);
            }
            finally
            {
                stream.Seek(0, 0);  // set the stream back to the begining
            }
        }

        public IEnumerable<SitemapRecord> GetLinks(CancellationTokenSource cancellationToken)
        {
            foreach (var item in _sitemapReader.GetLinks(_stream, cancellationToken))
                yield return item;
        }

        public DateTime GetLastMod(SitemapRecord item)
        {
            DateTime temp;
            DateTime dt =
                DateTime.TryParse(item.lastmod, out temp) ? temp :
                    (item.type == LocType.Sitemap ? GetLastModFromFilename(item.loc) : DateTime.MinValue);
            return dt;
        }

        public static Regex r = new Regex(@"(?:\?yyyy=|-|\/)(\d{4})(?:&mm=|-)(\d{2})(?:(?:&dd=|-)(\d{2}))?", RegexOptions.IgnoreCase);
        private DateTime GetLastModFromFilename(string loc)
        {
            int result;
            int[] numbers = { 0, 0, 0 };
            Match m = r.Match(loc);
            if (m.Success)
            {
                var listEnumerator = m.Groups.GetEnumerator();
                listEnumerator.MoveNext(); //Skip match
                for (var i = 0; listEnumerator.MoveNext() == true; i++)
                {
                    Group currentItem = (Group)listEnumerator.Current;
                    numbers[i] = int.TryParse(currentItem.Value, out result) ? result : 0;
                }
                var year = numbers[0];
                var month = numbers[1] == 0 ? 12 : numbers[1];
                var day = numbers[2] == 0 ? DateTime.DaysInMonth(year, month) : numbers[2];
                return new DateTime(year, month, day, 23, 59, 59);
            }
            else
                return DateTime.MinValue;
        }
    }
}
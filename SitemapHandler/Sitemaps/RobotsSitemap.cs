using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace SitemapHandler.Sitemaps
{
    public class RobotsSitemap : FlatSitemap
    {
        private const string SitemapRobotsTag = "Sitemap: ";

        public RobotsSitemap(string domain) : base(domain) { }

        public override IEnumerable<SitemapRecord> GetLinks(Stream stream, CancellationTokenSource cancelToken)
        {
            using (StreamReader reader = new StreamReader(stream))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                    if (line.StartsWith(SitemapRobotsTag, StringComparison.InvariantCultureIgnoreCase))
                        yield return new SitemapRecord { loc = line.Substring(SitemapRobotsTag.Length), type = LocType.Sitemap };
            }
        }
    }
}
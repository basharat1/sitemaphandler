using System.Threading;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace SitemapHandler.Sitemaps
{
    public class GZipUncompressor : SitemapReader
    {
        private SitemapReader _sReader;
        public GZipUncompressor(SitemapReader sReader) : base(sReader.Domain)
        {
            _sReader = sReader;
        }

        public override IEnumerable<SitemapRecord> GetLinks(Stream stream, CancellationTokenSource cancellationToken)
        {
            using (MemoryStream memory = new MemoryStream())
            using (var archive = new GZipStream(stream, CompressionMode.Decompress))
            {
                archive.CopyTo(memory);  //ToDo: Is there a way to process the stream as bytes are coming avoiding to put all the data at once in memory?
                memory.Position = 0;
                foreach (var item in _sReader.GetLinks(memory, cancellationToken))
                    yield return item;
            }
        }
    }
}
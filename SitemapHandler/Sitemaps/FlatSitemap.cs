using System.Threading;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace SitemapHandler.Sitemaps
{
    public class FlatSitemap : SitemapReader
    {
        protected static readonly string[] tagValues = new[] { "loc", "lastmod" };
        protected static readonly string[] tagParents = new[] { "sitemap", "url" };
        public FlatSitemap(string domain) : base(domain) { }

        public override IEnumerable<SitemapRecord> GetLinks(Stream stream, CancellationTokenSource cancelToken)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Ignore;
            using (XmlReader r = XmlReader.Create(stream, settings))
            {
                r.MoveToContent();
                Dictionary<string, string> elementValues = new Dictionary<string, string>();

                while (r.Read() && !cancelToken.IsCancellationRequested)
                {
                    if (XmlNodeType.Element == r.NodeType && tagParents.Contains(r.Name))
                        elementValues.Clear();

                    if (XmlNodeType.Element == r.NodeType)
                        elementValues["lastElement"] = r.Name;

                    if (XmlNodeType.Text == r.NodeType && (tagValues.Contains(elementValues["lastElement"])))
                        elementValues[elementValues["lastElement"]] = r.Value;

                    if (XmlNodeType.EndElement == r.NodeType && tagParents.Contains(r.Name) && elementValues.ContainsKey("loc"))
                    {
                        var loc = elementValues["loc"];
                        var url = new Uri(loc);
                        //if (url.Host == Domain)
                        //{
                            yield return new SitemapRecord
                            {
                                loc = loc,
                                lastmod = elementValues.ContainsKey("lastmod") ? elementValues["lastmod"] : null,
                                type = r.Name == "sitemap" ? LocType.Sitemap : LocType.Url
                            };
                       // }
                        elementValues.Clear();
                    }
                }
            }
        }
    }
}
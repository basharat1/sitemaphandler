using System.Text.Json.Serialization;
using System.Collections.Generic;
using System;

namespace SitemapHandler.Entities
{
    public class Client
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool? IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
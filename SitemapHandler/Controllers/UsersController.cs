using System.Net.Mail;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using SitemapHandler.Model;
using SitemapHandler.Validators;
using SitemapHandler.Services;

namespace SitemapHandler.Controllers
{
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IAuthenticateService _userService;

        public UsersController(IAuthenticateService userService)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        [HttpPost("oauth/token")]
        public async Task<ActionResult<AuthenticateResponse>> Authenticate(AuthenticateRequest model)
        {
            var validator = new AuthenticateValidator();
            var validation_result = validator.Validate(model);
            if (!validation_result.IsValid)
                return BadRequest(new ErrorResponse(Constants.ApiErrors.VALIDATION_ERROR,
                                                    "SignIn Validation Errors",
                                                    validation_result.Errors));

            var response = await _userService.Authenticate(model);

            if (response == null)
                return BadRequest(new ErrorResponse(Constants.ApiErrors.INVALID_LOGIN, "Username or password is incorrect"));

            return Ok(response);
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SitemapHandler.Helpers;
using SitemapHandler.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SitemapHandler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SiteMap : ControllerBase
    {
        // post: api/<SiteMap>
        [HttpPost]
        //[Authorize]
        public async Task<object> Scrape(string url,string domain, DateTime since , DateTime until)
        {
            Uri myUri = new Uri(domain.ToString());

            SiteMapService _SiteMapService = new SiteMapService();
            
             var data= await _SiteMapService.Scrape(url.ToString(),myUri, since, until);
            
           return JsonConvert.SerializeObject(data);
        }

    }
}

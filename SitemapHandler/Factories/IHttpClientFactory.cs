using System.Net.Http;
using System;
using System.Collections.Generic;

namespace SitemapHandler.Factories
{
    public interface IHttpClientFactory
    {
        HttpClient GetHttpClient(string AcceptEncoding);
    }
}
using SitemapHandler.Helpers;
using System.Collections.Generic;
using System.Net.Http;


namespace SitemapHandler.Factories
{
    public class HttpClientFactory : IHttpClientFactory
    {
        public HttpClient GetHttpClient(string AcceptEncoding)
        {
            var client = new HttpClient(new SocketsHttpHandler());
            client.DefaultRequestHeaders.UserAgent.ParseAdd(BrowserHelper.GetUserAgentString());
            //client.DefaultRequestHeaders.Accept.ParseAdd("text/html, application/xhtml+xml, application/xml; q = 0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
            client.DefaultRequestHeaders.Accept.ParseAdd("text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
            if (string.IsNullOrEmpty(AcceptEncoding))
            {
                client.DefaultRequestHeaders.AcceptEncoding.ParseAdd("*");
            }
            else
            {
                client.DefaultRequestHeaders.AcceptEncoding.ParseAdd(AcceptEncoding);
            }
            //client.DefaultRequestHeaders.AcceptEncoding.ParseAdd("gzip");
            //client.DefaultRequestHeaders.AcceptEncoding.ParseAdd("deflate");
            //client.DefaultRequestHeaders.AcceptEncoding.ParseAdd("br");

            //            client.DefaultRequestHeaders.AcceptEncoding.ParseAdd("identity");
            client.DefaultRequestHeaders.AcceptLanguage.ParseAdd("en-US,en;q=0.9");
            return client;
        }
    }
}
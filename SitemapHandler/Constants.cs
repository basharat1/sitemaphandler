public class Constants
{
    public static class Issues
    {
        public const int SITEMAP_XML_INVALID = 10;
        public const int URL_RETRY = 11;
        public const int URL_INVALID = 12;
        public const int FETCH_TIMEOUT = 13;
        public const int NotFound = 14;
        public const int InternalServer = 15;
        public const int TooManyRequest = 16;

    }
    public static class ApiErrors
    {
        public const int SUCCESS = 100;
        public const int ADMIN_ONLY_ACCESS = 101;
        public const int INVALID_OIDC_JWT_TOKEN = 102;
        public const int DEALS_DW_UNAVAILABLE = 103;
        public const int ASINS_DW_UNAVAILABLE = 104;
        public const int ASINS_DW_ERROR = 105;
        public const int INVALID_LOGIN = 106;
        public const int RESTRICTED_ACCESS = 107;
        public const int NOT_LOGGED = 108;
        public const int VALIDATION_ERROR = 109;
        public const int FEE_UPLOAD_NOFILE = 110;
        public const int SIGNUP_DUPLICATE_EMAIL = 112;
        public const int NO_NEW_PASSWORD_PROVIDED = 113;
        public const int WRONG_USERID = 114;
        public const int INVALID_OLD_PASSWORD = 115;
        public const int INVALID_RESETPWD_TOKEN = 116;
        public const int INVALID_CSV = 117;
        public const int DEALS_UPLOAD_NOFILE = 118;
        public const int SIGNUP_DUPLICATE_STOREID = 119;

        public const int UNEXPECTED_ERROR = 500;
        public const int Un_Authorized = 403;

    }

}

public enum LocType
{
    Sitemap,
    Url
}

public static class Enums
{
    public enum CsvKeyHeaders
    {
        Asin,
        DateShipped,
        Revenue,
        Title,
        ParentAsin,
        ProductLink,
        Category,
        Geo,
        DealPrice,
        DealStart,
        DealEnd
    }
}
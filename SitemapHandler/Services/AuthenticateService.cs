using System.Collections.Generic;
using System.IO;
using System.Dynamic;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

using SitemapHandler.Model;
using SitemapHandler.Entities;
using Npgsql;
using Dapper;
using SitemapHandler.Helpers;

namespace SitemapHandler.Services
{
    public interface IAuthenticateService
    {
        Task<AuthenticateResponse> Authenticate(AuthenticateRequest model);
        // IEnumerable<User> GetAll();
    }

    public class AuthenticateService : IAuthenticateService
    {
        // users hardcoded for simplicity, store in a db with hashed passwords in production applications
        private readonly AppSettings _appSettings;

        //private static readonly FluidParser _parser = new FluidParser();

        public AuthenticateService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
       // string Secret = "kCwHrCI7gSCFRxptldc88A5LbNsww6CV";

        public async Task<AuthenticateResponse> Authenticate(AuthenticateRequest model)
        {
            using var connection = new NpgsqlConnection(_appSettings.ConnectionString);

            var client = await connection.QueryFirstOrDefaultAsync<Client>
                ("SELECT * FROM clients WHERE password is NOT NULL AND password = @password AND username = @username",
                new { username = model.Username, password = model.Password });

            //string user = "kCwHrCI7gSCFRxptldc88A5LbNsww6CV";
            //string Password = "kCwHrCI7gSCFRxptldc88A5LbNsww6CV";
            if (client == null) return null;

                // authentication successful so generate jwt token
                var token = generateJwtToken(client);

                return token;
                //return new AuthenticateResponse(token);

        }
        private AuthenticateResponse generateJwtToken(Client client)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", client.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var Expires= tokenDescriptor.Expires;
            var _token= tokenHandler.WriteToken(token);
            //return tokenHandler.WriteToken(token);
            return new AuthenticateResponse(_token, Expires);
        }
    }
}
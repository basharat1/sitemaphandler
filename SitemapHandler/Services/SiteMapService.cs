﻿
using Microsoft.Extensions.Logging;
using SitemapHandler.Factories;
using SitemapHandler.Model;
using SitemapHandler.Sitemaps;
using System.Collections.Concurrent;
using System.Threading;
using System.Xml;
namespace SitemapHandler.Services
{
    public class SiteMapService
    {
        protected ConcurrentDictionary<Uri, ScrapingInternalData> _internalData;
        protected ConcurrentDictionary<Uri, ScrapingInternalData> _internalData1;

        private ConcurrentDictionary<string, byte> _alreadyProcessedUris = new ConcurrentDictionary<string, byte>();
        private readonly ILogger _logger;
        private Thread _thread = null;
        static SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);
        private CancellationTokenSource cancelToken = new CancellationTokenSource();
        public Factories.IHttpClientFactory HttpClientFactory { get; set; }
        public enum StatusOptions
        {
            Idle,
            Running,
        }
        public StatusOptions Status { get; private set; }

        public SiteMapService()
        {
            //ToDo: Improve logger implementation according to best practices.
            var factory = LoggerFactory.Create(b => b.AddSimpleConsole(formatterOptions =>
            {
                formatterOptions.SingleLine = true;
            }));
            _logger = factory.CreateLogger<SiteMapService>();

            Status = StatusOptions.Idle;
            _internalData = new ConcurrentDictionary<Uri, ScrapingInternalData>();
            _internalData1 = new ConcurrentDictionary<Uri, ScrapingInternalData>();

            HttpClientFactory = new HttpClientFactory();

        }
        public async Task<ConcurrentDictionary<Uri, ScrapingInternalData>> Scrape(string url,Uri domain, DateTime since, DateTime until)
        {

            if (since == DateTime.MinValue)
                since = DateTime.MinValue;
            if (until==DateTime.MinValue)
                until = DateTime.MaxValue;

             _internalData[domain] = new ScrapingInternalData(domain);
             _internalData1[domain] = new ScrapingInternalData(domain);

            EnqueueSitemap($"{url.ToString()}", domain, since, until);
            //EnqueueSitemap($"{domain.ToString()}sitemap.xml", domain, since, until);
            //EnqueueSitemap($"{domain.ToString()}sitemap_index.xml", domain, since, until);
            //EnqueueSitemap($"{domain.ToString()}sitemap.xml.gz", domain, since, until);
            //EnqueueSitemap($"{domain.ToString()}sitemap.gz", domain, since, until);
            //EnqueueSitemap($"{domain.ToString()}robots.txt", domain, since, until);


            return OnStart();
            //semaphoreSlim.Wait();
            //try
            //{
            //    if (_thread == null)
            //    {
            //        _thread = new Thread(new ThreadStart(OnStart));
            //        _thread.IsBackground = false;
            //        _thread.Start();
            //    }
            //}
            //finally
            //{
            //    semaphoreSlim.Release();
            //}
        }
        private void EnqueueSitemap(string url, Uri domain, DateTime since, DateTime until)
        {
            if (_alreadyProcessedUris.ContainsKey(url))
                return;
            _alreadyProcessedUris[url] = (byte)0;

            _logger.LogInformation($"{Task.CurrentId}-Enqueueing sitemap {url}");
            try
            {
                SitemapUrl _url = new SitemapUrl(url, domain, since, until);
                _internalData[domain].Queue.Enqueue(_url);
            }
            catch (System.UriFormatException)
            {
                //HandleIssue(new ScrapingIssue
                //{
                //    Code = Constants.Issues.SITEMAP_XML_INVALID,
                //    Domain = domain,
                //    Description = $"Invalid URI: {url}"
                //});
            }
        }
        List<Task> tasks = new List<Task>(); 

        private ConcurrentDictionary<Uri, ScrapingInternalData> OnStart()
        {
            int value;
            int maxConcurrency = int.TryParse(Environment.GetEnvironmentVariable("SCRAPER_CONCURRENT_TASK_COUNT"), out value) ? value : Environment.ProcessorCount;
            _logger.LogInformation($"Maximum Concurrency: {maxConcurrency}");
            int index = 0;
            _logger.LogInformation($"Starting thread: {Thread.CurrentThread.ManagedThreadId}");
            using (SemaphoreSlim concurrencySemaphore = new SemaphoreSlim(maxConcurrency))
            {
                while (_internalData.Keys.Count > 0 && !cancelToken.IsCancellationRequested)
                {
                    try { concurrencySemaphore.Wait(cancelToken.Token); }
                    catch (OperationCanceledException ex) 
                    {
                        break; 
                    }

                    var t = Task.Run(() =>
                    {
                        try
                        {
                            index = (index + 1) % _internalData.Count;
                            Uri domain = _internalData.Keys.ElementAt(index);
                            var domainData = _internalData[domain];
                            var domainSemaphore = domainData.DomainSemaphore;
                            //var domainSemaphore = DomainSemaphore;

                            //6<=5 


                            //if (domainData.Queue.Count <= maxConcurrency)
                                domainSemaphore.Wait(cancelToken.Token);
                            domainData.IncreaseTaskCount();
                            try { ProcessDomain(domain); }
                            finally
                            {
                                var tasks = domainData.DecreaseTaskCount();
                                if (domainSemaphore.CurrentCount == 0 && tasks == 0)
                                    domainSemaphore.Release();
                            }
                        }
                        catch (DivideByZeroException ex) 
                        {
                        }

                        catch (ArgumentOutOfRangeException ex)
                        {
                            //Do Nothing... just retry
                            _logger.LogError($"Method Onstart(ArgumentOutOfRangeException)  Exception: {ex.Message} ");

                        }
                        finally { concurrencySemaphore.Release(); }
                    });
                    tasks.Add(t);
                }
                _logger.LogInformation($"{Thread.CurrentThread.ManagedThreadId}-Waiting All Tasks");
                Task.WaitAll(tasks.ToArray());
                
            }
            //if (cancelToken.IsCancellationRequested)
            //    HandleCancel();
            //else
            //    HandleFinish();
            _alreadyProcessedUris.Clear();
            //_previouslyDatedProcessedUrls.Clear();
            _internalData.Clear();
            //_DynamicHeaders.Clear();
            Status = StatusOptions.Idle;
            _thread = null;
            cancelToken = new CancellationTokenSource();
            return _internalData1;

        }
        private void ProcessDomain(Uri domain)
        {
            if (!_internalData.ContainsKey(domain)) return;

            var queue = _internalData[domain].Queue;
            if (queue.TryDequeue(out ScrapingUrl result))
            {
                _logger.LogInformation($"{Task.CurrentId}-{domain.ToString()} queue Count={queue.Count}");
                if (result is SitemapUrl)
                    ProcessSitemap((SitemapUrl)result);
                //if (result is DealUrl)
                //    ProcessUrl((DealUrl)result);
            }
            else
            {
                //HandleSiteScraped(_internalData[domain]);
                ScrapingInternalData temp;
                _internalData.Remove(domain, out temp);
            }
        }
        protected void ProcessSitemap(SitemapUrl url)
        {
            string AcceptEncoding = null;
            //foreach (var a in _DynamicHeaders)
            //{
            //    if (url.Domain.OriginalString.ToLower().Contains(a.Key.ToLower()))
            //    {
            //        AcceptEncoding = a.Value;
            //    }
            //}
            _logger.LogInformation($"{Task.CurrentId}-Processing sitemap {url.ToString()} - {url.Since} | {url.Until}");
            using (HttpClient client = HttpClientFactory.GetHttpClient(AcceptEncoding))
            {
                try
                {
                    HttpResponseMessage response = client.GetAsync(url.ToString(), cancelToken.Token).GetAwaiter().GetResult();
                    if (response.IsSuccessStatusCode)
                    {
                        Stream stream = response.Content.ReadAsStream();
                        Handler shandler = new Handler(stream, url);
                        foreach (SitemapRecord item in shandler.GetLinks(cancelToken))
                        {
                            if (cancelToken.IsCancellationRequested) break;
                            DateTime dt = shandler.GetLastMod(item);
                            _logger.LogInformation($"{Task.CurrentId}-Checking {item.loc}-{item.lastmod}({dt}) from {url.ToString()}");
                            bool inTime = dt >= url.Since && dt <= url.Until;
                            if (item.type == LocType.Sitemap)
                            {
                                if (dt == DateTime.MinValue || inTime)
                                    EnqueueSitemap(item.loc, url.Domain, url.Since, url.Until);
                            }
                            else if (item.type == LocType.Url && Convert.ToString(item.loc).EndsWith(".xml"))
                            {
                                if (dt == DateTime.MinValue || inTime)
                                    EnqueueSitemap(item.loc, url.Domain, url.Since, url.Until);
                            }
                            else if (inTime)
                            {

                                EnqueueUrl(item.loc, url.Domain, dt);

                            }
                        }
                    }
                }
                catch (XmlException xe)
                {
                    _logger.LogError(xe, $"Invalid XML: {url.ToString()}");
                    //HandleIssue(new ScrapingIssue
                    //{
                    //    Code = Constants.Issues.SITEMAP_XML_INVALID,
                    //    Description = $"Invalid XML: {url.ToString()}",
                    //    Domain = url.Domain,
                    //    Uri = url
                    //});
                }
                catch (System.Exception e)
                {
                    //if (!cancelToken.IsCancellationRequested)
                    //    RetryUrl(url, e);
                }
            }
        }
        private void EnqueueUrl(string url, Uri domain, DateTime urlModifiedDate)
        {
            if (_alreadyProcessedUris.ContainsKey(url))
                return;
            _alreadyProcessedUris[url] = (byte)0;

            _logger.LogInformation($"{Task.CurrentId}-Enqueueing url {url}");
            try
            {
                DealUrl _url = new DealUrl(url, domain, urlModifiedDate);
                _internalData[domain].Queue.Enqueue(_url);
                _internalData1[domain].Queue.Enqueue(_url);
                _internalData1[domain].Count++;

            }
            catch (System.UriFormatException)
            {
                //HandleIssue(new ScrapingIssue
                //{
                //    Code = Constants.Issues.URL_INVALID,
                //    Domain = domain,
                //    Description = $"Invalid URI: {url}"
                //});

            }


        }
        private void ProcessUrl(DealUrl url)
        {
            //_logger.LogInformation($"{Task.CurrentId}-Puppeteer {url.ToString()}");
            _logger.LogInformation($"{Task.CurrentId}-Puppeteer {url.ToString()}");


            try
            {
            }
            catch (Exception ex)
            {

            }




        }


    }
}
